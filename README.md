### Trabalho de Graduação FATEC - SJC

## Titulo: Geolocalizador de Túmulos

1. INTRODUÇÃO

Quem nunca precisou visitar um ente querido falecido no cemitério e enfrentou dificuldades em localizar o túmulo dessa pessoa? essa dificuldade sempre foi muito comum de acontecer, as vezes a pessoa que perdemos é de outra cidade, e no instante em que foi visitar, encontrar o túmulo em meios a tantos outros não é uma tarefa fácil, ou ainda conhecidos com os quais nem sabemos ao certo que está lá. 
No ano de 2020 em que se inicia esse projeto, tivemos uma terrível pandemia da COVID - 19, com o qual gerou muitas mortes e muitos enterros e com o qual as pessoas próximas não podem acompanhar. 
Por outro lado, é possível observar as dificuldades enfrentadas pela falta visibilidade dos órgãos municipais e estaduais que administram os cemitérios. O planejamento de um cemitério municipal necessita conhecimento sobre as estruturas, calcular de forma adequada a taxa de ocupação e disponibilidade de espaços físicos para sepultamento.
Existem dois principais tipos de cemitérios, os horizontais que são os tradicionais (parques ou jardins) e os verticais, que normalmente é um edifício de um ou mais pavimentos. Esse tipo de problema com relação a espaço normalmente ocorre em cemitérios horizontais, localizado em áreas descobertas, no qual as sepulturas são identificadas por um lápide, ao nível do chão, e de pequenas dimensões
A Geolocalização sempre foi um meio de criar ferramentas e soluções que utilizamos hoje no nosso cotidiano. Assim foi usado para nos guiar em trajeto ou localizar objetos em um mapa, o funcionamento baseia-se na conexão de vários satélites que emitem sinais coletados e registrador por um terminal, a partir desses dados, o terminal identifica a latitude, longitude e altitude, determinando a localização dos objetos.
Tudo começo na década de 70, com o objetivo de localizar submarinos, quando o departamento de defesa dos Estados Unidos, criou em 1978 o NAVSTAR (Navigation System with Timing and Ranging), tinha o objetivo de garantir a precisão de entrega de armas e ao mesmo tempo ser único e eficiente. Uma das utilidades do NAVSTAR, por exemplo, era monitorar explosões nucleares dentro de sistemas de navegação do Exército americano.
Uma grande limitação da época, no entre o final do século XX, era que a precisão que entregavam para o GPS, sofria uma limitação de acurácia. Após mudança feita pelo presidente Bill Clinton que revogou o projeto e aumentou a acurácia para uso civil e comercial de 100 m para 20 m. Sendo assim, o uso do GPS segue sem intenção de restrições, onde tornou possível ser cada vez mais preciso.
 Atualmente o GPS possui múltiplas funcionalidades, podemos destacar tais como a defesa e segurança nacional, uso comercial, civil e científico, rotas de um trajeto de viagem e cálculos de tempo de velocidade e distância.
	São três elementos que fazem o funcionamento do GPS ao redor do mundo. Espacial, Controladora e Utilizadora. Que podemos classificar como:

•	Espacial: 24 Satélites distribuídos em 6 planos orbitais, onde circulam a 20.200km de altura e circundam a Terra a cada 12 horas
•	Controladora: Ambientes terrestres que fazem monitoria e mantém as atividades dos satélites.
•	Utilizadora: Receptores, que recebem o sinal do satélite e fazem o cálculo da posição no momento exato.

Até com que essa tecnologia alcançasse os smartfones, foram criados aprimoramentos de hardware inovadores, que se tornou cada vez mais acessível, fazendo assim hoje a possibilidade de grande maioria possuir GPS integrado.
Essa evolução aos acessos, aumentou as possibilidades de se criar aplicações que integram o uso do GPS. Com isso também é permitido obter dados de localização de determinados objetos e registrá-los em uma base de dados com a descrição de um objeto tal como ele é, para o reconhecimento em um mapa. Por exemplo, o aplicativo Uber, ele expõe a localização do veículo e a descrição dele, inclusive os dados do motorista. Uma forma de aprimoramento da localização baseia-se em imagens, que podem estar vinculadas a localização do objeto no mapa.
Com a imersão de tal tecnologia, possibilita o mapeamento dos túmulos, obtendo os dados relativos de sua geolocalização. Onde os meios tradicionais, são sempre colocados como pontos de referências, em que muitas vezes são retirados ou alterados, ou em meio a tantos túmulos parecidos uns com os outros geram confusão em localizar. 
Além disso, as entidades responsáveis pela administração dos cemitérios municipais, possuem um trabalho de gestão de espaços para sepultamentos, podendo assim o mapeamento de tais túmulos contribuírem para essa gestão.
Como solução, um PWA – Progressive Web Application, que consome esses dados e projeta o mapa integrado com a localização do usuário e das devidas sepulturas, para contribuir com a localização dos túmulos em um cemitério. 
As entidades administrativas do cemitério, ao solicitarem o serviço de mapeamento do cemitério, enviariam os dados necessários através de uma API que pudesse receber os dados do falecido escolhido e em seguida os dados da localização, essa API dependeria dos meios de preenchimento desses dados. Um exemplo, em Jundiai em 2019 eles fizeram com a utilização de drones, mas poderia vir de um smartphone.
Outrossim, de forma colaborativa, através dos visitantes que quiserem colaborar com a localização de algum ente, pensando na usabilidade própria ou para algum outro conhecido.
1.1. Objetivos do Trabalho 
O objetivo geral deste trabalho é através do uso de geolocalização e o conceito PWA – Progressive Web Application, colaborar com o mapeamento de cemitérios, trazendo facilidades na localização de sepulturas.

1.2. Conteúdo do Trabalho
O presente trabalho está estruturado em seis Capítulos, cujo conteúdo é sucintamente apresentado a seguir:
No Capítulo 2 é feita a fundamentação das tecnologias.
O Capítulo 3 apresenta o desenvolvimento da solução.
No Capítulo 4 são apresentados os resultados.
O Capítulo 5 apresenta as considerações finais  deste trabalho a partir da análise dos resultados obtidos...

2. FUNDAMENTAÇÃO TÉCNICA
2.1. PWA – Progressive Web App
	O acesso à internet teve processos evolutivos tecnológicos, que mudaram as necessidades dos usuários e suas experiências de uso ao decorrer do tempo. Com a vinda dos smartfones isso torna ainda mais claro, pois possuem diversos recursos integrados e com eles vieram aplicativos nativos, necessários para utilização desses recursos.
	Com tantos aplicativos que foram surgindo no mercado é possível notar que cabe ao usuário realizar diversos download, de apps que abririam poucas vezes. Fazendo com que ocupe espaço em seu smartfone.
	Sendo assim, surgiu o PWA – Progressive Web App, ele é um site que acessa navegadores móveis e interage aos recursos do smarfone e fornece uma interface como se os usuários estivessem em um aplicativo.
	Além disso, o PWA colabora no processo de desenvolvimento pois utilizam linguagens que são interpretadas pelo navegador como CSS, HTML e JavaScript. Outrossim, o desenvolvedor não precisa se preocupar em desenvolver uma aplicação para cada sistema operacional, ou até mesmo dispositivos específicos. Mas somente se aqueles componentes com os quais ele está desenvolvendo será compatível com o navegador presente no dispositivo e isso atualmente está sendo cada vez mais tendo menos compatibilidades.

	Alguns recursos de possíveis utilização em um PWA são como:
•	Notificação em push
•	Icone integrado na tela do dispositivo
•	Rodagem em segundo plano
•	Funcionamento em modo Offiline
•	Acesso a recursos de mecanismos
o	Galerias de fotos
o	Câmera
o	Geolocalização
o	Contatos


Vantagens e Desvantagens PWA

Vantagens	Desvantagens
Linguagens comuns: HTML/JS/CSS	Ausência nas lojas de aplicativos
Envio de notificação	Sem integração com vibração e toque
Sem necessidade de Download	Sem integração com outros apps
Facilidade de alterações	Cross-browser: diferentes reações em diferentes navegadores
Acesso a aplicações nativas	Aplicações dependentes de performance
Aplicações responsivas	Legitimidades: o formato pode gerar desconfiança em alguns usuários
	


2.2. Mapeamento Colaborativo

